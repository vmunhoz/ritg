﻿using UnityEngine;
using System.Collections;

public class PilotController : MonoBehaviour {

    public bool AI = true;
    public Pilot pilot = null;

    public float distanceTotalPoints = 0f;
    public float distanceNextPoints = 0f;

    // Use this for initialization
    void Start () {
        if (AI)
        {
            pilot = new Pilot();
        }
        else
        {
            pilot = new Pilot("Player");
        }
        
	}

    internal string getDistancePoints()
    {
        return (distanceTotalPoints - distanceNextPoints).ToString();
    }

}
