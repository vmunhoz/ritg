﻿using UnityEngine;
using System.Collections;
using System;

public class TrackController : MonoBehaviour {

    public static TrackController Instance { get; protected set; }

    // The only tile sprite we have right now, so this
    // it a pretty simple way to handle it.
    public Sprite floorSprite;
    //public GameObject prefab001;
    //public GameObject prefab111;
    public GameObject prefab050;

    //public Collider2D[] Trackers { get; internal set; }
    public ArrayList trackers = new ArrayList();

    // The world and tile data
    public Track Track { get; protected set; }

    // Use this for initialization
    void Start()
    {
        if (Instance != null)
        {
            Debug.LogError("There should never be two world controllers.");
        }
        Instance = this;

        // Create a world with Empty tiles
        Track = new Track();

        // Create a GameObject for each of our tiles, so they show visually. (and redunt reduntantly)
        /*
        for (int x = 0; x < Track.Width; x++)
        {
            for (int y = 0; y < Track.Height; y++)
            {
                // Get the tile data
                Tile tile_data = Track.GetTileAt(x, y);

                // This creates a new GameObject and adds it to our scene.
                GameObject tile_go = new GameObject();
                tile_go.name = "Tile_" + x + "_" + y;
                tile_go.transform.position = new Vector3(tile_data.X, tile_data.Y, 0);
                tile_go.transform.SetParent(this.transform, true);

                // Add a sprite renderer, but don't bother setting a sprite
                // because all the tiles are empty right now.
                tile_go.AddComponent<SpriteRenderer>();

                // Use a lambda to create an anonymous function to "wrap" our callback function
                tile_data.RegisterTileTypeChangedCallback((tile) => { OnTileTypeChanged(tile, tile_go); });
            }
        }
        */

        // Shake things up, for testing.
        //Track.RandomizeTiles();

        GenerateNewTrack();

        Instance.transform.localScale = new Vector3(6f, 6f, 1f);

        foreach (TrackSession session in Track.Sessions)
        {
            if (session == null)
            {
                break;
            }

            
        }
    }

    public void instantiateTrackSession()
    {

        Tile tile_data = Track.GetTileAt(Track.placeX, Track.placeY);

        TrackSession session = Track.Sessions[Track.currentSession];

        UnityEngine.Object pPrefab = Resources.Load("TrackSessions/" + session.xIn+"-"+session.yIn+"-"+session.angleOut); // note: not .prefab!

        GameObject tile_go = (GameObject)Instantiate(pPrefab, new Vector3(tile_data.X, tile_data.Y, 0), Quaternion.identity);
        tile_go.name = "Tile_" + tile_data.X + "_" + tile_data.Y;
        tile_go.transform.position = new Vector3(tile_data.X, tile_data.Y, 0);

        tile_go.transform.Rotate(new Vector3(0, 0, (Track.angleIn * -90) ));
        //tile_go.transform.rotation = new Quaternion(0, 0, Track.nextAngle * -90, 1.0f);
        tile_go.transform.SetParent(this.transform, true);

        if (tile_go.GetComponentInChildren<Tracker>())
        {
            trackers.Add(tile_go.GetComponentInChildren<Tracker>().gameObject.GetComponent<Collider2D>());
        }

        // Use a lambda to create an anonymous function to "wrap" our callback function
        tile_data.RegisterTileTypeChangedCallback((tile) => { OnTileTypeChanged(tile, tile_go); });
    }

    public void instantiateFirstSession()
    {

        Tile tile_data = Track.GetTileAt(Track.startX, Track.startY);

        GameObject prefab = prefab050;    

        GameObject tile_go = (GameObject)Instantiate(prefab, new Vector3(tile_data.X, tile_data.Y, 0), Quaternion.identity);
        tile_go.name = "Tile_" + tile_data.X + "_" + tile_data.Y;
        tile_go.transform.position = new Vector3(tile_data.X, tile_data.Y, 0);

        tile_go.transform.Rotate(new Vector3(0, 0, (Track.angleIn * -90) ));
        //tile_go.transform.rotation = new Quaternion(0, 0, Track.nextAngle * -90, 1.0f);
        Debug.Log(tile_go.transform.rotation);
        tile_go.transform.SetParent(this.transform, true);

        // Use a lambda to create an anonymous function to "wrap" our callback function
        tile_data.RegisterTileTypeChangedCallback((tile) => { OnTileTypeChanged(tile, tile_go); });
    }

    public void GenerateNewTrack()
    {

        int antiCrash = 0;
        int maxSessions = 30;

        Track.Sessions[0] = Track.GenerateStartSession();
        instantiateFirstSession();

        int angleCount = 3;
        int angle = 0;

        for (int i = 1; i < maxSessions; i++)
        {
            //check if is in the caution zone. 
            //If it is in the caution zone, call another function to generate cautiously

            Track.Sessions[i] = Track.GenerateNewTrackSession(angle);

            antiCrash = 0;
            while (Track.crashed)
            {
                Track.ResetLastTrackSession();
                Track.Sessions[i] = Track.GenerateNewTrackSession(angle);
                antiCrash++;

                if (antiCrash > 5)
                {
                    Debug.LogError("CANT PLACE SESSION, STILL CRASHING");
                    Track.crashed = false;
                }
            }

            instantiateTrackSession();

            if (Track.finished)
            {
                i = maxSessions;
            }

            angleCount++;
            if (angleCount > 3)
            {
                angle = makeCurveToTheEnd();
                angleCount = 0;

            }
            else
            {
                if (IsItGoingToCrash())
                    angle = makeCurveToTheEnd();
                else
                {
                    if (Track.getDistanceFromStart() <= 3)
                        angle = makeCurveToTheEnd();
                    else
                        angle = 0;
                }
                    
            }
            
            
        }

    }

    internal ArrayList getAllTrackers()
    {
        return trackers;
    }

    public int makeCurveToTheEnd()
    {
        // destination = Track.StartX , Track.StartY-1;

        int diffX = Track.nextX - Track.startX;
        int diffY = Track.nextY - (Track.startY-1);

        if (Track.angleOut == 0)
        {
            if (diffX == 0 && diffY == 0)
                return 0;

            if (diffX > 0)
                return 3;
            else if (diffX < 0)
                return 1;
            else if (diffY > 0)
                return 1;
        }
        else if (Track.angleOut == 2)
        {
            if (diffX == 0 && diffY == 0)
                Debug.LogError("Isso não deveria acontecer");

            if (diffX > 1)
                return 1;
            else if (diffX < 1)
                return 3;
            else if (diffY < 0)
                return 1;
        }
        else if (Track.angleOut == 1)
        {
            if (diffX == 0 && diffY == 0)
                return 3;

            if (diffY > 0)
                return 1;
            else if (diffY < 0)
                return 3;
            else if (diffX > 0)
                return 1;
        }
        else if (Track.angleOut == 3)
        {
            if (diffX == 0 && diffY == 0)
                return 1;

            if (diffY > 0)
                return 3;
            else if (diffY < 0)
                return 1;
            else if (diffX < 0)
                return 3;
        }

        return 0;
    }

    public bool IsItGoingToCrash()
    {
        Tile tile = null;
        switch (Track.angleOut)
        {
            case 0:
                for (int i = Track.nextY ; i < Track.nextY + 7; i++)
                {
                    tile = Track.GetTileAt(Track.nextX, i);
                    if (tile.Type == Tile.TileType.Floor)
                    {
                        Debug.LogWarning("VAI BATER! - " + Track.nextX + "," + i);
                        return true;
                    }
                }
                break;
            case 1:
                for (int i = Track.nextX; i < Track.nextX + 7; i++)
                {
                    tile = Track.GetTileAt(i, Track.nextY);
                    if (tile.Type == Tile.TileType.Floor)
                    {
                        Debug.LogWarning("VAI BATER! - " + i + "," + Track.nextY);
                        return true;
                    }
                }
                break;
            case 2:
                for (int i = Track.nextY; i > Track.nextY - 7; i--)
                {
                    tile = Track.GetTileAt(Track.nextX, i);
                    if (tile.Type == Tile.TileType.Floor)
                    {
                        Debug.LogWarning("VAI BATER! - " + Track.nextX + "," + i);
                        return true;
                    }
                }
                break;
            case 3:
                for (int i = Track.nextX; i > Track.nextX - 7; i--)
                {
                    tile = Track.GetTileAt(i, Track.nextY);
                    if (tile.Type == Tile.TileType.Floor)
                    {
                        Debug.LogWarning("VAI BATER! - " + i + "," + Track.nextY);
                        return true;
                    }
                }
                break;
            default:
                break;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // This function should be called automatically whenever a tile's type gets changed.
    void OnTileTypeChanged(Tile tile_data, GameObject tile_go)
    {

        if (tile_data.Type == Tile.TileType.Floor)
        {
            tile_go.GetComponent<SpriteRenderer>().sprite = floorSprite;
        }
        else if (tile_data.Type == Tile.TileType.Empty)
        {
            tile_go.GetComponent<SpriteRenderer>().sprite = null;
        }
        else
        {
            Debug.LogError("OnTileTypeChanged - Unrecognized tile type.");
        }


    }

    /// <summary>
    /// Gets the tile at the unity-space coordinates
    /// </summary>
    /// <returns>The tile at world coordinate.</returns>
    /// <param name="coord">Unity World-Space coordinates.</param>
    public Tile GetTileAtWorldCoord(Vector3 coord)
    {
        int x = Mathf.FloorToInt(coord.x);
        int y = Mathf.FloorToInt(coord.y);

        return Track.GetTileAt(x, y);
    }

}
