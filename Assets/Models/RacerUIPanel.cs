﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RacerUIPanel : MonoBehaviour {

    private string pos;
    private string name;

    public Text posText;
    public Text nameText;
    public Image bgImage;

    public bool player = false;

    public void setPos(string newPos)
    {
        pos = newPos;
        posText.text = pos;
    }

    public void setName(string newName)
    {
        name = newName;
        nameText.text = name;
    }

    public void setPlayer()
    {
        bgImage.color = new Color(0.055f, 0f, 0.72f, 0.3f);
        player = true;
    }

    public void setAI()
    {
        bgImage.color = new Color(0f, 0f, 0f, 0.3f);
        player = false;
    }
}
