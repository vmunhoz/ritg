﻿using UnityEngine;
using System.Collections;

public class Thruster {

    private int thrusterIndex;
    private GameObject obj;
    private ParticleSystem particle;
    private ParticleSystem.EmissionModule emission;

    public int ThrusterIndex
    {
        get
        {
            return thrusterIndex;
        }

        set
        {
            thrusterIndex = value;
        }
    }

    public GameObject Obj
    {
        get
        {
            return obj;
        }

        set
        {
            obj = value;
            Particle = obj.GetComponentInChildren<ParticleSystem>();
            Emission = Particle.emission;
            emission.enabled = false;
        }
    }

    public ParticleSystem Particle
    {
        get
        {
            return particle;
        }

        set
        {
            particle = value;
        }
    }

    public ParticleSystem.EmissionModule Emission
    {
        get
        {
            return emission;
        }

        set
        {
            emission = value;
        }
    }

    public bool isEnabled()
    {
        return this.obj.activeInHierarchy;
    }

    public void enable()
    {
        if (!emission.enabled)
        {
            emission.enabled = true;
        }
        /*
        if (!this.obj.activeInHierarchy)
        {
            this.obj.SetActive(true);
        }
        */
    }

    public void disable()
    {
        if (emission.enabled)
        {
            emission.enabled = false;
        }

        /*
        if (this.obj.activeInHierarchy)
        {
            this.obj.SetActive(false);
        }
        */
    }
}
