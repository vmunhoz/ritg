﻿using UnityEngine;
using System.Collections;
using System;

public class Track {

    // A two-dimensional array to hold our tile data.
    Tile[,] tiles;
    TrackSession[] sessions;

    public int startX = 20;
    public int startY = 20;
    public int placeX = 20;
    public int nextX = 20;
    public int previousPlaceX = 20;
    public int previousNextX = 20;
    public int placeY = 20;
    public int nextY = 20;
    public int previousPlaceY = 20;
    public int previousNextY = 20;
    //public int previousAngle = 0;

    public int angleIn = 0;
    public int previousAngleIn = 0;
    public int angleOut = 0;
    public int previousAngleOut = 0;

    public int currentSession = 0;

    public bool crashed = false;
    public bool finished = false;


    // The tile width of the world.
    public int Width { get; protected set; }

    // The tile height of the world
    public int Height { get; protected set; }

    public TrackSession[] Sessions
    {
        get
        {
            return sessions;
        }

        set
        {
            sessions = value;
        }
    }

    

    /// Initializes a new instance of the Track class.
    public Track(int width = 100, int height = 100)
    {
        Width = width;
        Height = height;

        tiles = new Tile[Width, Height];
        sessions = new TrackSession[30];

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                tiles[x, y] = new Tile(this, x, y);
            }
        }

        Debug.Log("Track created with " + (Width * Height) + " tiles.");
    }


    /// A function for testing out the system
    public void RandomizeTiles()
    {
        Debug.Log("RandomizeTiles");
        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {

                if (UnityEngine.Random.Range(0, 2) == 0)
                {
                    tiles[x, y].Type = Tile.TileType.Empty;
                }
                else
                {
                    tiles[x, y].Type = Tile.TileType.Floor;
                }

            }
        }
    }


    /// Gets the tile data at x and y.
    public Tile GetTileAt(int x, int y)
    {
        if (x > Width || x < 0 || y > Height || y < 0)
        {
            Debug.LogError("Tile (" + x + "," + y + ") is out of range.");
            return null;
        }
        return tiles[x, y];
    }

    public void ResetLastTrackSession()
    {
        currentSession -= 1;
        angleIn = previousAngleIn;
        angleOut = previousAngleOut;
        nextX = previousNextX;
        nextY = previousNextY;
        placeX = previousPlaceX;
        placeY = previousPlaceY;
        crashed = false;
    }

    public TrackSession GenerateNewTrackSession(int angle = 1, int counter = 0)
    {
        counter++;
        currentSession += 1;

        //backup!
        int previousAngle = angle;
        previousAngleIn = angleIn;
        previousAngleOut = angleOut;
        previousNextX = nextX;
        previousNextY = nextY;
        previousPlaceX = placeX;
        previousPlaceY = placeY;

        //int previousAngle = nextAngle;
        angleIn = angleOut;
        placeX = nextX;
        placeY = nextY;

        TrackSession s = new TrackSession(angle, getDistanceFromStart());
        angle = s.angleOut;

        // Ajustando angulo de saida
        angleOut = angleIn + angle;
        if (angleOut > 3)
        {
            angleOut = angleOut - 4;
        }

        // Ajustando Direção
        if (angleIn == 0)
        {
            nextX = placeX + s.xIn;
            nextY = placeY + s.yIn;
        }
        else if (angleIn == 1)
        {
            nextX = placeX + s.yIn;
            nextY = placeY - s.xIn;
        }
        else if (angleIn == 2)
        {
            nextX = placeX - s.xIn;
            nextY = placeY - s.yIn;
        }
        else if (angleIn == 3)
        {
            nextX = placeX - s.yIn;
            nextY = placeY + s.xIn;
        }

        // Ajustando Offset
        if (angleOut == 0)
        {
            nextY += 1;
        }
        else if (angleOut == 1)
        {
            nextX += 1;
        }
        else if (angleOut == 2)
        {
            nextY -= 1;
        }
        else if (angleOut == 3)
        {
            nextX -= 1;
        }

        /*
        if (getDistanceFromStart() == 0)
        {
            finished = true;
            return s;
        }
        */


        if (paintTiles(s))
        {
            Debug.Log(s.xIn + " , " + s.yIn + " - " + "angleIn: " + angleIn + " angleOut: " + angleOut);
            if (nextX == startX && nextY == startY)
            {
                finished = true;
            }
            return s;
        }
        else
        {
            Debug.Log("reset!");
            if (counter >= 20)
            {
                Debug.LogError("Mais de 10 resets");
                return s;
            }
            ResetLastTrackSession();
            return GenerateNewTrackSession(previousAngle, counter);
        }

    }

    public int getDistanceFromStart()
    {
        int distanceX = Math.Abs(startX - placeX);
        int distanceY = Math.Abs(startY-1 - placeY);
        return (distanceX + distanceY);
    }

    public TrackSession GenerateStartSession()
    {
        //int previousAngle = nextAngle;
        TrackSession s = new TrackSession(0,5,0);
        nextX = nextX + s.xIn;
        nextY = nextY + s.yIn + 1;
        angleOut = 0;

        Debug.Log(s.xIn + " , " + s.yIn + " - " + "angleIn: " + angleIn + " angleOut: " + angleOut);
        paintTiles(s);
        return s;
    }

    public bool paintTiles(TrackSession s)
    {
        Tile tile = null;
        int menorX = 0;
        int menorY = 0;
        int maiorX = 0;
        int maiorY = 0;

        int multiplier = 1;
        if (angleIn == 2 || angleIn == 3)
        {
            multiplier = -1;
        }

        switch (angleIn)
        {
            case 2:
                Debug.Log("--------------------------------------------- caso 2");
                Debug.Log("placeX = " + placeX);
                Debug.Log("s.xIn = " + s.xIn);
                Debug.Log("multiplier = " + multiplier);
                Debug.Log("placeY = " + placeY);
                Debug.Log("s.yIn = " + s.yIn);

                // X ===========================================
                if (placeX < placeX + (s.xIn * multiplier))
                {
                    menorX = placeX;
                    maiorX = placeX + (s.xIn * multiplier);
                }
                else
                {
                    menorX = placeX + (s.xIn * multiplier);
                    maiorX = placeX;
                }

                // Y ===========================================
                if (placeY < placeY + (s.yIn * multiplier))
                {
                    menorY = placeY;
                    maiorY = placeY + (s.yIn * multiplier);
                }
                else
                {
                    menorY = placeY + (s.yIn * multiplier);
                    maiorY = placeY;
                }
                break;
            case 0:
                Debug.Log("--------------------------------------------- caso 0");
                Debug.Log("placeX = " + placeX);
                Debug.Log("s.xIn = " + s.xIn);
                Debug.Log("multiplier = " + multiplier);
                Debug.Log("placeY = " + placeY);
                Debug.Log("s.yIn = " + s.yIn);
                // X ===========================================
                if (placeX < placeX + (s.xIn*multiplier))
                {
                    menorX = placeX;
                    maiorX = placeX + (s.xIn * multiplier);
                } else
                {
                    menorX = placeX + (s.xIn * multiplier);
                    maiorX = placeX;
                }

                // Y ===========================================
                if (placeY < placeY + (s.yIn * multiplier))
                {
                    menorY = placeY;
                    maiorY = placeY + (s.yIn * multiplier);
                } else
                {
                    menorY = placeY + (s.yIn * multiplier);
                    maiorY = placeY;
                }
                break;
            case 1:
                Debug.Log("--------------------------------------------- caso 1");
                // X ===========================================
                if (placeX < placeX + (s.yIn * multiplier))
                {
                    menorX = placeX;
                    maiorX = placeX + (s.yIn * multiplier);
                }
                else
                {
                    menorX = placeX + (s.yIn * multiplier);
                    maiorX = placeX;
                }

                // Y ===========================================
                if (placeY < placeY + (s.xIn * multiplier))
                {
                    menorY = placeY;
                    maiorY = placeY + (s.xIn * multiplier);
                }
                else
                {
                    menorY = placeY + (s.xIn * multiplier);
                    maiorY = placeY;
                }
                break;
            case 3:
                Debug.Log("--------------------------------------------- caso 3");
                Debug.Log("placeX = " + placeX);
                Debug.Log("s.yIn = " + s.yIn);
                Debug.Log("multiplier = " + multiplier);
                Debug.Log("placeY = " + placeY);
                Debug.Log("s.xIn = " + s.xIn);
                // X ===========================================
                if (placeX < placeX + (s.yIn * multiplier))
                {
                    menorX = placeX;
                    maiorX = placeX + (s.yIn * multiplier);
                }
                else
                {
                    menorX = placeX + (s.yIn * multiplier);
                    maiorX = placeX;
                }

                // Y ===========================================
                if (placeY < placeY + (s.xIn * multiplier))
                {
                    menorY = placeY;
                    maiorY = placeY + (s.xIn * multiplier);
                }
                else
                {
                    menorY = placeY + (s.xIn * multiplier);
                    maiorY = placeY;
                }
                break;
        }

        for (int x = menorX; x <= maiorX; x++)
        {
            for (int y = menorY; y <= maiorY; y++)
            {
                tile = GetTileAt(x, y);
                if (tile.Type == Tile.TileType.Floor)
                {
                    crashed = true;
                    Debug.LogWarning("CRASH: " + x + "," + y);
                    return false;
                }
            }

            if (!crashed)
            {
                for (int y = menorY; y <= maiorY; y++)
                {
                    tile = GetTileAt(x, y);
                    tile.Type = Tile.TileType.Floor;
                    //tile.RegisterTileTypeChangedCallback((tile_data) => { OnTileTypeChanged(tile_data, tile_go); });
                    Debug.Log("painted: " + x + "," + y);
                }
            }
            
        }

        return true;

    }

    public bool willCrashSoon()
    {
        return false;
    }
}
