﻿using UnityEngine;
using System.Collections;

public class TrackSession {

    private int XIn;
    private int YIn;
    private int AngleOut;
    private int size;

    public int xIn
    {
        get
        {
            return XIn;
        }

        set
        {
            XIn = value;
        }
    }

    public int yIn
    {
        get
        {
            return YIn;
        }

        set
        {
            YIn = value;
        }
    }

    public int angleOut
    {
        get
        {
            return AngleOut;
        }

        set
        {
            AngleOut = value;
        }
    }

    public TrackSession(int angle, int requiredSize = 0)
    {
        //int[,] possibility = new int[5, 1];

        TrackSession[] possibilities = new TrackSession[6];
        possibilities[0] = new TrackSession(0,0,0);
        possibilities[1] = new TrackSession(0,5,0,6);
        possibilities[2] = new TrackSession(1,1,1, 4);
        possibilities[3] = new TrackSession(-1, 1, 4);
        possibilities[4] = new TrackSession(0,0,1);
        possibilities[5] = new TrackSession(0,0,3);
        //possibilities[5] = new TrackSession(0,1,1);
        //possibilities[6] = new TrackSession(1,0,1);


        //Tile[,]  tiles = new Tile[Width, Height];
        //ArrayList possibilities = new ArrayList();
        //possibilities.Add(1);

        /*
        xIn = Random.Range(1, 1);
        yIn = Random.Range(1, 1);
        angleOut = angle;
        */

        bool liberate = false;
        int num = 0;

        while (!liberate)
        {
            num = Random.Range(0, possibilities.Length);
            if (possibilities[num].angleOut == angle && possibilities[num].size <= requiredSize)
                liberate = true;
        }
        

        xIn = possibilities[num].xIn;
        yIn = possibilities[num].yIn;
        angleOut = possibilities[num].angleOut;
    }

    public TrackSession(int x, int y, int angle, int size = 0)
    {
        xIn = x;
        yIn = y;
        angleOut = angle;
        this.size = size;
    }
}
