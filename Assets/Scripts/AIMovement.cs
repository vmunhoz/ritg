﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIMovement : MonoBehaviour {

    public PilotController pilot = null;

	public float speed				= 5.0f;
	public float reverseSpeed		= 5.0f;
	public float turnSpeed			= 0.3f;
	public float currentSpeed       = 0.0f;
	
	private float moveDirection		= 0.0f;
	private float turnDirection		= 0.0f;

    //public float distanceTotalPoints = 0f;
    //public float distanceNextPoints = 0f;
    public Collider2D lastTarget = null;
    public Collider2D nextTarget = null;
    public TrackController trackController;

    private ArrayList trackers;

    //AI VARIABLES
    private int curving				= 0;
	private bool stucked			= false;
    private Rigidbody2D rigidBody;
	
	// Use this for initialization
	void Start () 
	{
		rigidBody = GetComponent<Rigidbody2D> ();
        pilot = GetComponent<PilotController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		if (!stucked && (curving == 0 || curving > 30 || curving < 5)) {
			Accelerate();
		}

        updateDistanceNextPoints();

    }

    internal string getDistancePoints()
    {
        return (pilot.distanceTotalPoints - pilot.distanceNextPoints).ToString();
    }

    void updateDistanceNextPoints()
    {
        if (nextTarget)
        {
            pilot.distanceNextPoints = Vector2.Distance(nextTarget.transform.position, this.transform.position);
        }
    }

    void OnTriggerStay2D(Collider2D target)
    {

        if (target.tag == "Tracker")
        {
            //Debug.LogError("Tracker colidido");

            if (!nextTarget)
            {
                nextTarget = getNextTracker();
            }

            if (target == nextTarget)
            {
                pilot.distanceTotalPoints += 100;
                lastTarget = target;
                nextTarget = getNextTracker();
            }
        }

    }

    void stuck()
    {
        stucked = true;
    }

    void unstuck()
    {
        stucked = false;
    }


	float GetCurrentSpeed(){
		float x = Mathf.Abs (rigidBody.velocity.x);
		float y = Mathf.Abs (rigidBody.velocity.y);
		return x + y;
	}

	bool CheckCurve() {
		return false;
	}

	void Accelerate(){
		moveDirection = 1 * speed;
		GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, moveDirection)) ;
	}

	public void CurveRight(){
		curving += 1;
		turnDirection = -1 * turnSpeed;
		GetComponent<Rigidbody2D>().AddTorque(turnDirection) ;
	}

	public void CurveLeft(){
		turnDirection = 1 * turnSpeed;
		GetComponent<Rigidbody2D>().AddTorque(turnDirection) ;
	}

	public void StopCurve(){
		curving = 0;
		//GetComponent<Rigidbody2D>().
	}

	public void Brake(){
		moveDirection = -1 * speed;
		GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, moveDirection)) ;
	}

    private Collider2D getNextTracker()
    {
        trackers = trackController.getAllTrackers();

        Debug.Log("getNextTracker()");

        if (lastTarget)
        {
            for (int i = 0; i < trackers.Count; i++)
            {
                if (lastTarget == (Collider2D)trackers[i])
                {
                    if (i + 1 < trackers.Count)
                    {
                        return (Collider2D)trackers[i + 1];
                    }
                    else
                    {
                        return (Collider2D)trackers[0];
                    }
                }
            }
        }
        else
        {
            return (Collider2D)trackers[0];
        }

        return (Collider2D)trackers[0];
    }


}
