﻿using UnityEngine;
using System.Collections;

public class BrakeDetector : MonoBehaviour {

	private AIMovement aiMovement;
	
	void Start () 
	{
		aiMovement = transform.parent.GetComponent<AIMovement>();
	}
	
	void OnTriggerStay2D(Collider2D target) {
		
		if (target.tag == "CurveRight" || target.tag == "CurveLeft") {
			if (aiMovement.currentSpeed > 2.5) {
				aiMovement.BroadcastMessage ("Brake");
			}
		}


    }
	
	void OnTriggerExit2D(Collider2D target) {
		

	}
}
