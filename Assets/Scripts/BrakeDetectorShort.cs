﻿using UnityEngine;
using System.Collections;

public class BrakeDetectorShort : MonoBehaviour {

	private AIMovement aiMovement;
	
	void Start () 
	{
		aiMovement = transform.parent.GetComponent<AIMovement>();
	}
	
	void OnTriggerStay2D(Collider2D target) {
		
		if (target.tag == "CurveRight" || target.tag == "CurveLeft") {
			aiMovement.BroadcastMessage ("Brake");
			aiMovement.BroadcastMessage("stuck");
		}

    }
	
	void OnTriggerExit2D(Collider2D target) {
		
		aiMovement.BroadcastMessage("unstuck");

    }
}
