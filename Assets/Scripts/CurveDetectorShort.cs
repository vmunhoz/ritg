﻿using UnityEngine;
using System.Collections;

public class CurveDetectorShort : MonoBehaviour {

	private AIMovement aiMovement;
	private Rigidbody2D rigidBody;
	private int curveLimit = 60;
	
	void Start () 
	{
		aiMovement = transform.parent.GetComponent<AIMovement>();
		rigidBody = transform.parent.GetComponent<Rigidbody2D> ();
	}

	void OnTriggerStay2D(Collider2D target) {



		if (target.tag == "CurveRight") {

            try
            {
                if (rigidBody.angularVelocity > curveLimit * -1)
                {
                    aiMovement.BroadcastMessage("CurveRight");
                }
            }
            catch (System.Exception)
            {
                Debug.LogError(rigidBody);
                throw;
            }
			
		} 

		if (target.tag == "CurveLeft") {
			if (rigidBody.angularVelocity < curveLimit) {
				aiMovement.BroadcastMessage ("CurveLeft");
			}
		}
	}

	void OnTriggerExit2D(Collider2D target) {
		
		if (target.tag == "CurveRight") {
			aiMovement.BroadcastMessage ("StopCurve");
		} 
		
		if (target.tag == "CurveLeft") {
			aiMovement.BroadcastMessage ("StopCurve");
		}
	}
}
