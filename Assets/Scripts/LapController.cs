﻿using UnityEngine;
using System.Collections;

public class LapController : MonoBehaviour {

	public int lapNumber			= -1;
	public int checkpointA 			= 0;
	public int checkpointB 			= 0;
	public int checkpointC 			= 1;

    public bool player = false;
    public RaceManager raceManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D target) {
		
		if(target.tag == "Finish")
		{
			if (checkpointB == 1 && checkpointC == 1) {
				lapNumber += 1;
				
				checkpointA = 1;
				checkpointB = 0;
				checkpointC = 0;

                if (player)
                {
                    raceManager.updateLaps(lapNumber);
                }
				
				Debug.Log (this.name + ": " + "New Lap! " + lapNumber);
			}
		}
		
		if(target.tag == "CheckpointB")
		{
			if (checkpointA == 1 && checkpointB == 0 && checkpointC == 0) {
				lapNumber -= 1;
				
				checkpointA = 0;
				checkpointB = 1;
				checkpointC = 1;
				
				//Debug.Log ("Reset Lap!");
			}
			
			
			if (checkpointA == 0 && checkpointB == 0 && checkpointC == 1) {
				checkpointA = 0;
				checkpointB = 1;
				checkpointC = 1;
			}
			
			
		}
		
		if(target.tag == "CheckpointC")
		{
			if (checkpointA == 1 && checkpointB == 0 && checkpointC == 0) {
				checkpointA = 0;
				checkpointB = 0;
				checkpointC = 1;
			}
		}
}
}
