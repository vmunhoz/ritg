﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour 
{
    public PilotController pilot = null;

    public float speed				= 10.0f;
	public float reverseSpeed		= 5.0f;
	public float turnSpeed			= 0.6f;
	public float currentSpeed       = 0.0f;

    public bool paused = true;

	public int lapNumber			= 0;
	public int checkpointA 			= 0;
	public int checkpointB 			= 0;
	public int checkpointC 			= 1;

    //public float distanceTotalPoints     = 0f;
    //public float distanceNextPoints      = 0f;
    public Collider2D lastTarget    = null;
    public Collider2D nextTarget    = null;

    public TrackController trackController;

	private float moveDirection		= 0.0f;
	private float turnDirection		= 0.0f;

    private Rigidbody2D rigidBody;

    private Thruster t1 = new Thruster();
    private Thruster t2 = new Thruster();
    private Thruster t3 = new Thruster();
    private Thruster t4 = new Thruster();
    private Thruster t5 = new Thruster();
    private Thruster t6 = new Thruster();

    private ArrayList trackers;

    public Thruster T1
    {
        get
        {
            return t1;
        }

        set
        {
            t1 = value;
        }
    }

    public Thruster T2
    {
        get
        {
            return t2;
        }

        set
        {
            t2 = value;
        }
    }

    public Thruster T3
    {
        get
        {
            return t3;
        }

        set
        {
            t3 = value;
        }
    }

    public Thruster T4
    {
        get
        {
            return t4;
        }

        set
        {
            t4 = value;
        }
    }

    public Thruster T5
    {
        get
        {
            return t5;
        }

        set
        {
            t5 = value;
        }
    }

    internal string getDistancePoints()
    {
        return (pilot.distanceTotalPoints - pilot.distanceNextPoints).ToString();
    }

    public Thruster T6
    {
        get
        {
            return t6;
        }

        set
        {
            t6 = value;
        }
    }

    // Use this for initialization
    void Start () 
	{
		rigidBody = GetComponent<Rigidbody2D> ();
        pilot = GetComponent<PilotController>();

        GameObject thrusters = transform.FindChild("Thrusters").gameObject;
        T1.Obj = thrusters.transform.FindChild("T1").gameObject;
        T2.Obj = thrusters.transform.FindChild("T2").gameObject;
        T3.Obj = thrusters.transform.FindChild("T3").gameObject;
        T4.Obj = thrusters.transform.FindChild("T4").gameObject;
        T5.Obj = thrusters.transform.FindChild("T5").gameObject;
        T6.Obj = thrusters.transform.FindChild("T6").gameObject;

        if (paused)
        {
            this.enabled = false;
        }

    }
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		currentSpeed = GetCurrentSpeed ();
		//GameObject text1 = GameObject.Find("Text");
		//text1.GetComponent<Text>().text = "Velocity: " + currentSpeed + "\nAngular Velocity = " + rigidBody.angularVelocity;


		if (Input.GetAxis("Vertical") > 0.0f) {
			moveDirection = Input.GetAxis ("Vertical") * speed;
			GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, moveDirection)) ;
            T4.enable();
		}

		turnDirection = -Input.GetAxis ("Horizontal") * turnSpeed;
		GetComponent<Rigidbody2D>().AddTorque(turnDirection) ;

        
        if (turnDirection > 0)
        {
            T2.enable();
            T5.enable();
            T3.disable();
            T6.disable();
        }

        if (turnDirection < 0)
        {
            T3.enable();
            T6.enable();
            T2.disable();
            T5.disable();
        }

        if (turnDirection == 0)
        {
            T2.disable();
            T5.disable();
            T3.disable();
            T6.disable();
        }

        if (Input.GetAxis ("Vertical") < 0.0f) {
			moveDirection = Input.GetAxis ("Vertical") * reverseSpeed;
			GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, moveDirection)) ;
            T1.enable();
            T4.disable();
        }

        if (Input.GetAxis("Vertical") == 0.0f)
        {
            T1.disable();
            T4.disable();
        }

        updateDistanceNextPoints();

    }

    void manageThrusters(int thrusterNumber, bool enable)
    {
        if (true)
        {

        }
    }

   
	float GetCurrentSpeed(){
		float x = Mathf.Abs (rigidBody.velocity.x);
		float y = Mathf.Abs (rigidBody.velocity.y);
		return x + y;
	}


    void updateDistanceNextPoints()
    {
        if (nextTarget)
        {
            pilot.distanceNextPoints = Vector2.Distance(nextTarget.transform.position, this.transform.position);
        }
    }

	void OnTriggerEnter2D(Collider2D target) {

        //var distance = Vector3.Distance(object1.transform.position, object2.transform.position);

        if (target.tag == "Tracker")
        {
            //Debug.LogError("Tracker colidido");

            if (!nextTarget)
            {
                nextTarget = getNextTracker();
            }

            if (target == nextTarget)
            {
                pilot.distanceTotalPoints += 100;
                lastTarget = target;
                nextTarget = getNextTracker();
            }
        }

        if (target.tag == "Finish")
		{
			if (checkpointB == 1 && checkpointC == 1) {
				lapNumber += 1;

				checkpointA = 1;
				checkpointB = 0;
				checkpointC = 0;

				Debug.Log ("New Lap!");
			}
		}

		if(target.tag == "CheckpointB")
		{
			if (checkpointA == 1 && checkpointB == 0 && checkpointC == 0) {
				lapNumber -= 1;
				
				checkpointA = 0;
				checkpointB = 1;
				checkpointC = 1;
				
				Debug.Log ("Reset Lap!");
			}


			if (checkpointA == 0 && checkpointB == 0 && checkpointC == 1) {
				checkpointA = 0;
				checkpointB = 1;
				checkpointC = 1;
			}


		}

		if(target.tag == "CheckpointC")
		{
			if (checkpointA == 1 && checkpointB == 0 && checkpointC == 0) {
				checkpointA = 0;
				checkpointB = 0;
				checkpointC = 1;
			}
		}

	}

    private Collider2D getNextTracker()
    {
        trackers = trackController.getAllTrackers();

        Debug.Log("getNextTracker()");

        if (lastTarget)
        {
            for (int i = 0; i < trackers.Count; i++)
            {
                if (lastTarget == ( Collider2D ) trackers[i])
                {
                    if (i+1 < trackers.Count)
                    {
                        return (Collider2D)trackers[i + 1];
                    }
                    else
                    {
                        return (Collider2D)trackers[0];
                    }
                }
            }
        } else
        {
            return (Collider2D)trackers[0];
        }

        return (Collider2D)trackers[0];
    }
}
