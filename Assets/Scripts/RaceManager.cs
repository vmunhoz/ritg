﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.SceneManagement;

public class RaceManager : MonoBehaviour {

    public bool running = false;
    public float timer = 3f;
    public int totalLaps = 3;

    public GameObject textTimer;
    public GameObject debug1;
    public GameObject finishedRaceScreen;
    public Text textLaps;
    public PlayerMovement playerMovement;
    public GameObject racersListPanel;

    public RacerUIPanel[] positions = null;

    public List<PilotController> order = new List<PilotController>();

    public AIMovement[] racersAIMovement;

	public void updateLaps(int lap)
    {
        if (lap <= totalLaps)
        {
            textLaps.text = lap + "/" + totalLaps;
        } else
        {
            running = false;
            finishedRaceScreen.SetActive(true);
        }
        
    }

	void Update () {

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            textTimer.GetComponent<Text>().text = (Mathf.FloorToInt(timer)+1).ToString();

            if (timer <= 0)
            {
                running = true;
                textTimer.SetActive(false);
                playerMovement.paused = false;
                playerMovement.enabled = true;
                foreach (AIMovement racer in racersAIMovement)
                {
                    racer.enabled = true;
                }
            }
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            //Application.LoadLevel(0);
            SceneManager.LoadScene(0);
        }

        updateUI();

	}

    void updateUI()
    {

        if (order.Count == 0)
        {
            foreach (AIMovement racer in racersAIMovement)
            {
                order.Add(racer.GetComponent<PilotController>());
            }

            order.Add(playerMovement.GetComponent<PilotController>());

            startRacersUI();
        }

        order = order.OrderBy(x => x.getDistancePoints()).ToList();

        /*
        order.Sort(delegate (GameObject a, GameObject b) {
            return (a.GetComponent<CharacterStats>().initiative).CompareTo(b.GetComponent<CharacterStats>().initiative);
        });
        */

        //debug1.GetComponent<Text>().text = playerMovement.getDistancePoints();

        //debug1.GetComponent<Text>().text = order[order.Count-1].GetComponent<PilotController>().pilot.name;

        for (int i = 0; i < positions.Length; i++)
        {
            string name = order[order.Count - 1 - i].GetComponent<PilotController>().pilot.name;
            positions[i].setName(name);

            // if new racer is a player (not AI)
            if (!order[order.Count - 1 - i].GetComponent<PilotController>().AI)
            {
                // and current slot is not a player
                if (!positions[i].player)
                {
                    positions[i].setPlayer();
                }
            }
            else
            {
                // if new racer is AI and current slot is a player
                if (positions[i].player)
                {
                    positions[i].setAI();
                }
            }
            

        }

    }

    private void startRacersUI()
    {
        //initialize  positions vector
        positions = new RacerUIPanel[order.Count];
        for (int i = order.Count; i > 0; i--)
        {
            racersListPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(racersListPanel.GetComponent<RectTransform>().sizeDelta.x, order.Count * 43.75f) ;

            // instantiate
            UnityEngine.Object pPrefab = Resources.Load("UI/RacerUIPanel"); // note: not .prefab!
            GameObject racerUI = (GameObject)Instantiate(pPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            racerUI.transform.SetParent(racersListPanel.transform, true);

            // populate fields
            racerUI.GetComponent<RacerUIPanel>().setName(order[i-1].gameObject.name);
            racerUI.GetComponent<RacerUIPanel>().setPos((order.Count - i + 1).ToString());

            positions[order.Count - i] = racerUI.GetComponent<RacerUIPanel>();
        }
    }
}
